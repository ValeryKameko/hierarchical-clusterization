package com.gitlab.valerykameko.hierarchicalclustering

abstract class HierarchyItem {
    abstract val maxDistance: Double

    abstract val ids: Array<Int>
}