package com.gitlab.valerykameko.hierarchicalclustering

data class HierarchyVector(val id: Int): HierarchyItem() {
    override val maxDistance = 0.0
    override val ids: Array<Int>
        get() = arrayOf(id)
}