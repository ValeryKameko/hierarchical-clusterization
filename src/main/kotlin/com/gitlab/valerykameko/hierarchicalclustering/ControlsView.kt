package com.gitlab.valerykameko.hierarchicalclustering

import javafx.beans.property.SimpleDoubleProperty
import javafx.beans.property.SimpleIntegerProperty
import javafx.geometry.Pos
import javafx.scene.Parent
import javafx.scene.control.Alert
import javafx.scene.control.Alert.AlertType
import javafx.scene.control.ButtonType
import javafx.scene.control.ScrollPane
import javafx.scene.control.TextFormatter
import javafx.scene.layout.GridPane
import javafx.scene.layout.Priority
import javafx.scene.text.Font
import javafx.scene.text.FontWeight
import javafx.scene.text.TextAlignment
import tornadofx.*
import kotlin.random.Random


class ControlsView: View() {
    private val SPACING = 10.0

    val countProperty = SimpleIntegerProperty(1)
    val compareValues = listOf("Min", "Max").asObservable()
    var tablePane: ScrollPane? = null
    val distanceMatrix = arrayListOf(arrayListOf(SimpleDoubleProperty(0.0)))
    var buildListener: ((distances: Array<Array<Double>>, order: Hierarchy.Order) -> Unit)? = null
    var order: Hierarchy.Order = Hierarchy.Order.MIN_ORDER

    override val root: Parent = vbox {
        hgrow = Priority.NEVER
        isFillWidth = true
        spacing = SPACING
        label("Controls") {
            textAlignment = TextAlignment.CENTER
            font = Font.font("Verdana", FontWeight.BOLD, 15.0)
        }
        hbox {
            spacing = SPACING
            alignment = Pos.CENTER
            label("Number of points")
            spinner(min = 1, max = 10, initialValue = 1, property = countProperty)
        }
        label("Distance matrix")
        scrollpane {
            tablePane = this
            content = table()
        }
        hbox {
            spacing = SPACING
            alignment = Pos.CENTER
            label("Comparator")
            spinner(compareValues) {
                valueProperty().addListener { observable, oldValue, newValue ->
                    order = if (newValue == compareValues.first()) {
                        Hierarchy.Order.MIN_ORDER
                    } else {
                        Hierarchy.Order.MAX_ORDER
                    }
                }
            }
        }
        button("Build clusterization") {
            alignment = Pos.CENTER
            action { buildClusterization() }
        }
        button("Generate") {
            alignment = Pos.CENTER
            action { generate() }
        }
    }

    private fun generate() {
        distanceMatrix.forEachIndexed { rowIndex, row ->
            row.forEachIndexed { colIndex, item ->
                item.set(Random.nextDouble(0.0, 100.0))
            }
        }
    }

    private fun buildClusterization() {
        val count = countProperty.intValue()
        val distances = Array(count) { rowIndex ->
            Array(count) { colIndex ->
                val value = distanceMatrix[rowIndex][colIndex].value
                if (value < 0) {
                    val alert = Alert(
                        AlertType.ERROR,
                        "Matrix is incorrect",
                        ButtonType.OK
                    )
                    alert.showAndWait()
                    return
                }
                value
            }
        }

        buildListener?.let { it(distances, order) }
    }

    init {
        countProperty.addListener { _, _, newValue ->
            resizeMatrix(newValue.toInt())
            tablePane?.content = table()
        }
    }

    private fun resizeMatrix(count: Int) {
        while (distanceMatrix.count() < count) {
            val newRow = (1..count).map { SimpleDoubleProperty(0.0) }.toCollection(ArrayList())
            distanceMatrix.add(newRow)
        }
        while (distanceMatrix.count() > count)
            distanceMatrix.remove(distanceMatrix.last())

        distanceMatrix.forEach { row ->
            while (row.count() < count) {
                row.add(SimpleDoubleProperty(0.0))
            }
            while (row.count() > count)
                row.remove(row.last())
        }
    }

    private fun floatFormatter(): TextFormatter<String> {
        return TextFormatter<String> { change ->
            if (change.text.matches(Regex("""[0-9\-.,+]*"""))) {
                change
            } else {
                null
            }
        }
    }

    private fun table(): GridPane {
        return gridpane {
            row {
                distanceMatrix.forEachIndexed { rowIndex, row ->
                    row.forEachIndexed { colIndex, item ->
                        val field = textfield(item) {
                            textFormatter = floatFormatter()
                            maxWidth = 50.0
                        }
                        add(field, colIndex, rowIndex)
                    }
                }
            }
        }
    }
}