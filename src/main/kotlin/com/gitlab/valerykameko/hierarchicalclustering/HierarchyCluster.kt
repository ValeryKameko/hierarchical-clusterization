package com.gitlab.valerykameko.hierarchicalclustering

import kotlin.math.max

class HierarchyCluster(
    val items: Array<HierarchyItem>,
    val distance: Double,
    val order: Hierarchy.Order
): HierarchyItem() {
    override val maxDistance: Double
        get() = max(distance, items.map { it.maxDistance }.max()!!)
    override val ids: Array<Int>
        get() = items.flatMap { it.ids.asList() }.toTypedArray()
}