package com.gitlab.valerykameko.hierarchicalclustering

import javafx.geometry.Point2D
import javafx.geometry.Rectangle2D
import javafx.scene.Parent
import javafx.scene.canvas.Canvas
import javafx.scene.canvas.GraphicsContext
import javafx.scene.layout.Priority
import javafx.scene.layout.StackPane
import tornadofx.*
import java.lang.Double.max

class DendrogramView: View() {
    private val OFFSET = 15.0
    private val TEXT_OFFSET = 10.0
    private val RADIUS = 4.0
    private val MAX_DISTANCE = 1.0

    private lateinit var canvas: Canvas
    private lateinit var stackPane: StackPane

    var hierarchy: HierarchyItem? = null
        set(value) {
            field = value
            redraw()
        }

    override val root: Parent = stackpane {
        stackPane = this
        minHeight = 600.0
        minWidth = 800.0
        hgrow = Priority.ALWAYS
        canvas {
            canvas = this
            redraw()
            widthProperty().bind(stackPane.widthProperty())
            heightProperty().bind(stackPane.heightProperty())
            widthProperty().addListener { _ ->  redraw() }
            heightProperty().addListener { _ ->  redraw() }
        }
    }

    fun redraw() {
        val gc = canvas.graphicsContext2D
        gc.clearRect(0.0, 0.0, canvas.width, canvas.height)

        gc.fill = c("white")
        gc.fillRect(0.0, 0.0, canvas.width, canvas.height)

        hierarchy?.let {
            val width = max(0.0, canvas.width - 2 * OFFSET)
            val height = max(0.0, canvas.height - 3 * OFFSET)
            val maxDistance = max(it.maxDistance, MAX_DISTANCE)
            drawHierarchy(gc, it, Rectangle2D(OFFSET, OFFSET, width, height), maxDistance)
        }
    }

    private fun drawHierarchy(context: GraphicsContext,
                              item: HierarchyItem,
                              rectangle: Rectangle2D,
                              maxDistance: Double): Point2D {
        return when (item) {
            is HierarchyCluster -> drawHierarchyCluster(context, item, rectangle, maxDistance)
            is HierarchyVector -> drawHierarchyVector(context, item, rectangle)
            else -> throw IllegalArgumentException("Wrong Hierarchy Item")
        }
    }

    private fun drawHierarchyCluster(context: GraphicsContext,
                                     cluster: HierarchyCluster,
                                     rectangle: Rectangle2D,
                                     maxDistance: Double): Point2D {
        var parts = cluster.items.map { it.ids.size.toDouble() }
        val point = clusterPoint(cluster, rectangle, maxDistance)
        val sum = parts.sum()
        parts = parts.map { it / sum }

        var part = 0.0
        cluster.items.forEachIndexed() { index, item ->
            val width = parts[index] * rectangle.width
            val minX = rectangle.minX + part * rectangle.width
            val newRectangle = Rectangle2D(minX, rectangle.minY, width, rectangle.height)

            val itemPoint = drawHierarchy(context, item, newRectangle, maxDistance)
            context.strokeLine(itemPoint.x, itemPoint.y, itemPoint.x, point.y)
            context.strokeLine(itemPoint.x, point.y, point.x, point.y)
            context.fill = c("black")

            part += parts[index]
        }
        context.fillOval(point.x - RADIUS / 2.0, point.y - RADIUS / 2.0, RADIUS, RADIUS)
        context.strokeText("d = ${cluster.distance}", point.x + TEXT_OFFSET, point.y)

        println("point = $point")

        return point
    }

    private fun drawHierarchyVector(context: GraphicsContext,
                                    vector: HierarchyVector,
                                    rectangle: Rectangle2D): Point2D {
        val point = Point2D((rectangle.maxX + rectangle.minX) / 2, rectangle.maxY + OFFSET)

        context.fill = c("black")
        context.fillOval(point.x - RADIUS / 2.0, point.y - RADIUS / 2.0, RADIUS, RADIUS)
        context.strokeText("${vector.id}", point.x + TEXT_OFFSET, point.y + TEXT_OFFSET)
        return point
    }

    private fun clusterPoint(cluster: HierarchyCluster, rectangle: Rectangle2D, maxDistance: Double): Point2D {
        return when (cluster.order) {
            Hierarchy.Order.MIN_ORDER -> Point2D(
                (rectangle.minX + rectangle.maxX) / 2,
                (1.0 - cluster.distance / maxDistance) * rectangle.height + rectangle.minY
            )
            Hierarchy.Order.MAX_ORDER -> Point2D(
                (rectangle.minX + rectangle.maxX) / 2,
                cluster.distance / maxDistance * rectangle.height + rectangle.minY
            )
        }
    }
}