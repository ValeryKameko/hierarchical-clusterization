package com.gitlab.valerykameko.hierarchicalclustering

object Hierarchy {
    enum class Order(val comparator: Comparator<Double>) {
        MIN_ORDER(Comparator.naturalOrder()),
        MAX_ORDER(Comparator.reverseOrder());
    }

    fun buildHierarchy(distances: Array<Array<Double>>,
                       order: Order): HierarchyItem {
        val size = distances.size
        val vectors: ArrayList<HierarchyItem> = (0 until size).map { HierarchyVector(it) }.toCollection(ArrayList())
        return buildHierarchy(distances, vectors, order)
    }

    private fun buildHierarchy(distances: Array<Array<Double>>,
                               items: ArrayList<HierarchyItem>,
                               order: Order): HierarchyItem {
        while (items.size > 1) {
            var cluster: HierarchyCluster? = null
            for (item1 in items) {
                for (item2 in items) {
                    if (item1 === item2)
                        continue
                    val itemsDistance = distance(distances, item1, item2, order)
                    val currentDistance = cluster?.distance

                    if (currentDistance == null || order.comparator.compare(itemsDistance, currentDistance) < 0)
                        cluster = HierarchyCluster(arrayOf(item1, item2), itemsDistance, order)
                }
            }
            items.removeAll(cluster!!.items)
            items.add(cluster)
        }
        return items.first()
    }

    private fun distance(distances: Array<Array<Double>>,
                         item1: HierarchyItem,
                         item2: HierarchyItem,
                         order: Order): Double {
        val ids1 = item1.ids
        val ids2 = item2.ids
        var distance: Double? = null

        ids1.forEach { id1 ->
            ids2.forEach { id2 ->
                if (distance == null || order.comparator.compare(distance, distances[id1][id2]) < 0) {
                    distance = distances[id1][id2]
                }
            }
        }
        return distance!!
    }
}