package com.gitlab.valerykameko.hierarchicalclustering

import javafx.scene.Parent
import tornadofx.View
import tornadofx.hbox
import tornadofx.plusAssign
import tornadofx.stackpane

class ApplicationView: View() {
    private val dendrogramView: DendrogramView by inject()
    private val controlsView: ControlsView by inject()

    override val root: Parent = stackpane {
        hbox {
            this += dendrogramView
            this += controlsView
        }
    }

    init {
        controlsView.buildListener = { distances, order ->
            dendrogramView.hierarchy = Hierarchy.buildHierarchy(distances, order)
        }
    }
}